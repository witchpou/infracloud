#!/bin/bash
set -eu

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml

kubectl apply -f /root/kubernetes-post-install-config/service-nodeport.yaml