# Hetzner access token (per project)
variable "hcloud_token" {}

variable "ssh_keypath" {} # public key to be used to access vms
variable "ssh_keyname" {} # key id in hetzner cloud

variable "ssh_keypath_nopassword" {} # public ssh key without pw to enable tf to run stuff
variable "ssh_private_key" {} # private ssh key to enable tf...

variable "node_image" {
  default = "ubuntu-18.04"
}

variable "master_type" {
  default = "cx21"
}

variable "worker_type" {
  default = "cx11"
}
variable "workers" {
  default = "0"
}
