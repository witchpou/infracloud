# Links

* for creating ingress controller, see https://github.com/nginxinc/kubernetes-ingress/blob/master/docs/installation.md
* for used example deployments, see https://github.com/nginxinc/kubernetes-ingress/tree/master/examples/complete-example

# Testing

curl --resolve cafe.example.com:$IC_HTTPS_PORT:$IC_IP https://cafe.example.com:$IC_HTTPS_PORT/coffee --insecure