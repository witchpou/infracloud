#!/bin/bash

############### deployments ###################

#deploy projectbuilder
kubectl apply -f ljprojectbuilder.yaml

#deploy cafe example
kubectl apply -f cafe.yaml

#deploy secrets for ingress contoller
kubectl apply -f cafe-secret.yaml

#deploy ingress
kubectl apply -f cafe-ingress.yaml