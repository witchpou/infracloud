# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

# Upload a new SSH key
resource "hcloud_ssh_key" "terraformremotekey" {
  name = "terraform remote access"
  public_key = "${file("${var.ssh_keypath_nopassword}")}"
}

resource "hcloud_network" "internalnet" {
  name = "internalnet"
  ip_range = "192.168.2.0/24"
}

resource "hcloud_network_subnet" "internalsubnet" {
  network_id = "${hcloud_network.internalnet.id}"
  type = "server"
  network_zone = "eu-central"
  ip_range   = "192.168.2.0/28"
}
