# infracloud

* execute terraform init
* execute terraform apply
* login to machine with ssh -i <<private key>> **root**@machine_url

# ingress controller
Ingress controller https://github.com/nginxinc/kubernetes-ingress is used for [ingress-ctrl](ingress-ctrl) and [app-deployment](app-deployment). Look at https://github.com/nginxinc/kubernetes-ingress/blob/master/docs/installation.md for ingress controller installation.

# debugging & troubleshooting

kubectl describe pod <coredns-pod-ids>
kubectl logs pod <coredns-pod-ids>

systemctl status kubelet
journalctl -xeu kubelet

